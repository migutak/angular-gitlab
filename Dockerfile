# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
  FROM tiangolo/node-frontend:10 as build-stage

  WORKDIR /app
  COPY package*.json /app/
  RUN npm install
  COPY ./ /app/
  ARG configuration=production

  RUN cd /app && npm run build --prod --configuration $configuration


  # Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
  FROM nginx:1.17.1-alpine

  RUN rm -rf /usr/share/nginx/html/*
  COPY --from=build-stage /app/dist /usr/share/nginx/html
  COPY /nginx-custom.conf /etc/nginx/nginx.conf

  EXPOSE 80

  # run nginx
  CMD ["nginx", "-g", "daemon off;"]

